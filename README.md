# T**-*D

  

This is a working prototype Docker container which produces and serves projects created by the Python tool [mkdocs](https://www.mkdocs.org/)

  

Gitlab CI is used to build and test that the container is built and functions as expected.

  

### Prerequisites

  

* Docker version 18.09.2 or later

* Valid mkdocs project

  

To build and run, Docker version 18.09.2 or later must be installed. There must be a working Internet connection to build the container.

  

Tested to be working on Docker version 18.09.2 for OS X and Amazon Linux 2. Windows is untested.

  

While the mkdocs tool is not a requirement for running the Docker container, an mkdocs project must be provided as input for both produce and serve functions.

  

## Build instructions

  

1. Download the latest version by using `git clone https://gitlab.com/otsu-td/td/`

2. Build the container with the command `docker build -t dmkdocs .`

* Note: You can pick any name you want and replace `dmkdocs` accordingly, but the usage of tags makes it easier to manage running Docker containers

  
  

## Running instructions
Basic example usage and execution is as follows:
```bash
docker run [-d] [-p localport:8000] [-v /localdir:/data] mkdocs [produce|serve|TEST]
```
`-p` is required when running `serve` to reach the container's webserver.
`-v /localdir:/data]` is required to mount a virtual volume for producing and serving the mkdocs project.
`-d` is optional to launch the container detached. Useful for the `serve` function.

## Available functions
### `produce`: Create .tar.gz file with mkdocs project

The container mounts a virtual volume as a container directory and creates a tarball suitable for distribution. The tarball will have the filename `mkdocs.tar.gz` and be in the same directory as the mounted virtual volume. If there already is an `mkdocs.tar.gz` file present, it will be overwritten so be careful!

  

The mounted virtual volume must contain a valid `mkdocs.yml` file. When mounting the virtual volume, `/data` must be the target Docker directory, like so: `-v [localdir]:/data`

  

Example usage:

``` bash

docker run -v $PWD:/data dmkdocs produce

```

  

### `serve`: Serve mkdocs project using `mkdocs.tar.gz` as input

The container mounts a virtual volume as a container directory, looks for the `mkdocs.tar.gz` tarball created by the `produce` function, unpacks, and presents it using the mkdocs internal webserver. The internal webserver uses `port 8000` by default; you need to bind a local port to Docker with `-p` to access the webserver outside of the container.
The mounted virtual volume must contain a valid `mkdocs.tar.gz` file, preferably created by the `produce` function. When mounting the virtual volume, `/data` must be the target Docker directory, like so: `-v [localdir]:/data`

Example usage (running Docker detached):
```bash
docker run -d -p 8000:8000 -v $PWD:/data dmkdocs serve
```

If your localhost is configured correctly, you can now access the mkdocs webserver serving your `mkdocs.tar.gz` file at [http://localhost:8000](http://localhost:8000)

**Hint**: The container won't shut itself down when serving. If you launch with the flag you can stop the container gracefully with the command
```bash 
docker stop $(docker ps | grep "dmkdocs" | awk '{print $1}')
```

(you can also use `kill` if you're in a hurry but it's less elegant, machines have feelings too)

  
  

### `TEST`: Verify functionality

The container uses [BATS](https://github.com/sstephenson/bats) to verify expected behavior. To run the unit tests built into the container, run the command
```bash
docker run dmkdocs TEST
```
 

## TODO:

* Verify for Windows
* Ability to specify custom names for the tarball and projects
* Unit tests to verify running webserver
* Scan directory for valid mkdocs projects and create individual tarballs for each project