FROM python:3.7
WORKDIR /usr/src/app
COPY app/ .
# clone and install bats for bash unit testing
RUN git clone https://github.com/sstephenson/bats
RUN bats/install.sh /usr/local

RUN pip install -r requirements.txt

# use entrypoint to allow for use of input parameters "produce" and "serve"
ENTRYPOINT [ "bash", "runner.sh" ]
