#!/bin/bash

# test function for unit testing
hello() {
    echo "Hello world!"
}

# creates an mkdocs.tar.gz file and puts it in the docker mounted virtual volume
produce() {
    if [ -e /data/mkdocs.yml ]
    then
        cd /data
        mkdocs build
        tar -zcvf mkdocs.tar.gz *
    else
        echo "no mkdocs.yml file in current directory, are you in your mkdocs project?"
    fi
}

# unpacks the mkdocs.tar.gz file produced by the produce function(),
# modifies the mkdocs.yml file to accept any incoming connection to port 8000
serve() {
    if [ -e /data/mkdocs.tar.gz ]
    then
        cd /data
        echo "found mkdocs.tar.gz!"
        tmpdir="/tmp/mkdocs$(date +%s)"
        mkdir $tmpdir && cp mkdocs.tar.gz $tmpdir
        cd $tmpdir
        tar -zxvf mkdocs.tar.gz

        sed -i '/^dev_addr/d' mkdocs.yml
        echo "" >> mkdocs.yml
        echo "dev_addr: 0.0.0.0:8000" >> mkdocs.yml

        mkdocs serve
    else
        echo "can't find mkdocs.tar.gz, have you mounted the virtual volume correctly?"
    fi 
}

"$@"