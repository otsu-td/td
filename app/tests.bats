#!/usr/bin/env bats

setup() {
    cwd="$(pwd)"
    mkdir /data
}

teardown() {
    cd $cwd
    rm -r /data
}

@test "test hello world function" {
    result="$(bash funcs.sh hello)"
    [[ "$result" == "Hello world!" ]]
}

@test "test make mkdocs default project" {
    cd $BATS_TMPDIR
    mkdocs new default
    result="$(cat default/mkdocs.yml)"
    [[ "$result" == "site_name: My Docs" ]]
    rm -r default 
}

@test "test produce() function" {
    mkdocs new default
    mv default/* /data 
    bash $cwd/funcs.sh produce
    test -f /data/mkdocs.tar.gz
    [[ "$?" -eq 0 ]]
}

@test "test function() where no mkdocs file exists" {
    run bash $cwd/funcs.sh produce
    echo $output | grep "no mkdocs.yml file in current directory, are you in your mkdocs project"
    [[ $? -eq 0 ]]
}