#!/bin/bash

echo 

# if the container is launched with TEST parameter, ensure nothing is deleted
# from /data or mounted volume by accident...
if [ $1 == "TEST" ]
then
    if [ -d /data ]
    then 
        echo "/data directory found, DON'T run tests
            * with docker virtual volume mounted to /data
            * locally with existing local /data directory
        ... as it will modify the /data directory during the test"
        exit 1
    fi
fi

# checks for which input parameter is given for docker run
case "$1" in
    TEST)
        bats tests.bats
        ;;
    serve)
        bash funcs.sh serve
        ;;
    produce)
        bash funcs.sh produce
        ;;
    *)
        echo "Usage: $0 [produce|serve|TEST]"
        ;;
esac

echo
exit 0